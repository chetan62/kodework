<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the "site-content" div and all content after.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
?>

	</div><!-- .site-content -->


	<footer id="colophon" class="site-footer" role="contentinfo">

	<?php $frontpage_id = get_option('page_on_front');?>
	

		<section class="business">
		    <div class="container-fluid">
		      <div class="row">
		        <div class="col-sm-6 one-col">
		        <figure class="fig">
		          <div class="img">
		            <img src="<?php the_field('footer_job_image', $frontpage_id); ?>" alt="" class="img-responsive">
		          </div>
		          <figcaption>
		            <div class="cap-table">
		              <div class="cap-wrap">
		                <h3><?php the_field('footer_job_heading', $frontpage_id); ?></h3>
		                <p class="txt"><?php the_field('footer_job_description', $frontpage_id); ?></p>
		                <p><a href="<?php the_field('footer_job_more_link', $frontpage_id); ?>" class="btn btn-info"><?php the_field('footer_job_more_label', $frontpage_id); ?></a></p>
		              </div>
		            </div>              
		          </figcaption>
		        </figure>
		        </div>
		        <div class="col-sm-6 two-col">
		        <figure class="fig">
		          <div class="img">
		            <img src="<?php the_field('footer_startup_image', $frontpage_id); ?>" alt="" class="img-responsive">
		          </div>
		          <figcaption>
		            <div class="cap-table">
		              <div class="cap-wrap">
		                <h3><?php the_field('footer_startup_heading', $frontpage_id); ?></h3>
		                <p class="txt"><?php the_field('footer_startup_description', $frontpage_id); ?></p>
		                <p><a href="<?php the_field('footer_startup_label_link', $frontpage_id); ?>" class="btn btn-info"><?php the_field('footer_startup_label', $frontpage_id); ?></a></p>
		              </div>
		            </div>              
		          </figcaption>
		        </figure>
		        </div>
		      </div>
		    </div>
		</section>
		

		<section class="foot-up">
		    <div class="container">
		      <div class="row">
		        <div class="col-sm-6 col-md-3 col-xs-12">
		        	<?php dynamic_sidebar( 'first-footer-widget-area' ); ?>
		        </div>
		        <div class="col-sm-6 col-md-3 col-xs-12">
		        	<?php dynamic_sidebar( 'second-footer-widget-area' ); ?>		          
		        </div>
		        <div class="col-sm-6 col-md-3 col-xs-12">
		        	<?php dynamic_sidebar( 'third-footer-widget-area' ); ?>
		        </div>
		        <div class="col-sm-6 col-md-3 col-xs-12">
		        	<?php dynamic_sidebar( 'fourth-footer-widget-area' ); ?>		          
		        </div>
		      </div>
		    </div>
		</section>

		<section class="foot-tech">
		    <div class="container">
		      <div class="row">
		        <div class="col-sm-12">
		          <div class="tech-prod">
		            <div class="item"><img class="img-responsive" src="<?php echo get_stylesheet_directory_uri(); ?>/img/magento-logo.png" alt=""></div>
		            <div class="item"><img class="img-responsive" src="<?php echo get_stylesheet_directory_uri(); ?>/img/wp-logo.png" alt=""></div>
		            <div class="item"><img class="img-responsive" src="<?php echo get_stylesheet_directory_uri(); ?>/img/woocommerce.png" alt=""></div>
		            <div class="item"><img class="img-responsive" src="<?php echo get_stylesheet_directory_uri(); ?>/img/magento-logo.png" alt=""></div>
		            <div class="item"><img class="img-responsive" src="<?php echo get_stylesheet_directory_uri(); ?>/img/wp-logo.png" alt=""></div>
		            <div class="item"><img class="img-responsive" src="<?php echo get_stylesheet_directory_uri(); ?>/img/woocommerce.png" alt=""></div>
		            <div class="item"><img class="img-responsive" src="<?php echo get_stylesheet_directory_uri(); ?>/img/wp-logo.png" alt=""></div>
		          </div>
		        </div>
		      </div>
		    </div>
		</section>

		<section class="foot-copy">
		    <div class="container">
		      <div class="row">
		      	<?php dynamic_sidebar( 'copy-footer-widget-area' ); ?>		        
		      </div>
		    </div>
		</section>

	</footer><!-- .site-footer -->

</div><!-- .site -->

	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?php echo get_stylesheet_directory_uri(); ?>/js/bootstrap.min.js"></script>      

    <script src="<?php echo get_stylesheet_directory_uri(); ?>/js/slick.min.js"></script>
    <script src="<?php echo get_stylesheet_directory_uri(); ?>/js/app.js"></script>

<?php wp_footer(); ?>

</body>
</html>

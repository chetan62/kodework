<?php
/**

 * Template name: Home Page

 *

 */

get_header(); ?>


<section class="hero cover">
  <div class="conatiner-fluid">
    <div class="row">
      <div class="col-sm-12">        
        <img src="<?php the_field('hero_image');?>" alt="" class="img-responsive center-block">
      </div>
    </div>
  </div>
</section>

<section class="dark introline">
  <div class="container">
    <div class="row">
      <div class="col-sm-9 col-md-10 col-xs-12">
        <div>
          <h1 class="h3"><?php the_field('intro_title');?></h1>
        </div>
      </div>
      <div class="col-sm-3 col-md-2 col-xs-12">
        <div class="text-right more">
          <a href="<?php the_field('intro_label_link');?>" class="btn btn-primary"><?php the_field('intro_section_label');?></a>
        </div>
      </div>
    </div>
  </div>
</section>







<?php get_footer(); ?>
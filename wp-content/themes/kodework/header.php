<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<link rel="icon" href="<?php echo get_stylesheet_directory_uri(); ?>/img/favicon.png">

	<!-- Custom fonts -->
    <link href='http://fonts.googleapis.com/css?family=Lato:400,700|Ubuntu:400,500,700' rel='stylesheet' type='text/css'>

	<!-- Bootstrap -->
	<link href="<?php echo get_stylesheet_directory_uri(); ?>/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo get_stylesheet_directory_uri(); ?>/css/slick.css" rel="stylesheet">
    <link href="<?php echo get_stylesheet_directory_uri(); ?>/css/slick-theme.css" rel="stylesheet">
    <link href="<?php echo get_stylesheet_directory_uri(); ?>/css/style.css" rel="stylesheet">
    <link href="<?php echo get_stylesheet_directory_uri(); ?>/css/app.css" rel="stylesheet">


	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<!--[if lt IE 9]>
	<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/html5.js"></script>
	<![endif]-->
	<?php wp_head(); ?>
</head>

<body <?php body_class('site-body'); ?>>
<div id="page" class="hfeed site">

	<header class="site-head">

	  <div class="htop visible-md visible-lg">
	    <div class="container">
	      <div class="row">
	        <div class="col-sm-6">
	          <div class="left">
				<?php dynamic_sidebar( 'first-header-widget-area' ); ?>
	          </div>              
	        </div>
	        <div class="col-sm-6 text-right">
	          <div class="right">
	          	<?php dynamic_sidebar( 'second-header-widget-area' ); ?>
	          </div>
	          <div class="flag">
	          	<?php dynamic_sidebar( 'flag-header-widget-area' ); ?>
	          </div>
	        </div>
	      </div>
	    </div>
	  </div>

	  <div class="hbottom" data-spy="affix" data-offset-top="38">
	    <nav class="navbar navbar-default">
	      <div class="container">
	        <!-- Brand and toggle get grouped for better mobile display -->
	        <div class="navbar-header">
	          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#main-nav-collapse" aria-expanded="false">
	            <span class="sr-only">Toggle navigation</span>
	            <span class="icon-bar"></span>
	            <span class="icon-bar"></span>
	            <span class="icon-bar"></span>
	          </button>
	          <a class="navbar-brand" href="index.php">
	            <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/kw-logo.jpg" alt="Kodework" class="img-responsive">
	          </a>
	        </div>

	        <?php
	            wp_nav_menu( array(
	                'menu'              => 'primary',
	                'theme_location'    => 'primary',
	                'depth'             => 2,
	                'container'         => 'div',
	                'container_class'   => 'collapse navbar-collapse',
	        		'container_id'      => 'main-nav-collapse',
	                'menu_class'        => 'nav navbar-nav navbar-right',
	                'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
	                'walker'            => new wp_bootstrap_navwalker())
	            );
	        ?>

	        
	      </div><!-- /.container\ -->
	    </nav>
	  </div>

	</header>



	<div id="content" class="site-content">

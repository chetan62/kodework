$(window).scroll(function(){
    if ( $( ".hbottom" ).hasClass( "affix" ) ) {

	    $( "body.home" ).addClass("sticky-on");
	    $( "body.home" ).removeClass("sticky-off");
	 
	}

	if ( $( ".hbottom" ).hasClass( "affix-top" ) ) {

	    $( "body.home" ).addClass("sticky-off");
	    $( "body.home" ).removeClass("sticky-on");
	 
	}
});


$(document).ready(function(){

	// slick slider initialize code
	$('.tech-prod').slick({
		dots: false,
		arrows: false,
		infinite: false,
		speed: 300,
		slidesToShow: 7,
		slidesToScroll: 1,
		responsive: [
		{
		  breakpoint: 1024,
		  settings: {
		    slidesToShow: 4,
		    slidesToScroll: 3,
		    infinite: true
		  }
		},
		{
		  breakpoint: 600,
		  settings: {
		    slidesToShow: 3,
		    slidesToScroll: 3
		  }
		},
		{
		  breakpoint: 480,
		  settings: {
		    slidesToShow: 2,
		    slidesToScroll: 2
		  }
		}
		// You can unslick at a given breakpoint now by adding:
		// settings: "unslick"
		// instead of a settings object
		]
	});


});